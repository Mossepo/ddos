CC = g++
CPP = cpp
CFLAGS = -std=c++11 -g -Wall -Wextra -Wconversion -Wsign-conversion -pedantic
LDFLAGS = -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system
SRC = $(wildcard *.cpp)
OBJDIR = obj
OBJ = $(SRC:%.cpp=$(OBJDIR)/%.o)
DEPDIR = dep
DEP = $(OBJ:$(OBJDIR)/%.o=$(DEPDIR)/%.d)

bin/ddos: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

-include $(DEP)

$(DEPDIR)/%.d: %.cpp
	@$(CPP) $(CFLAGS) $< -MM -MT $(@:$(DEPDIR)/%.d=%.o) >$@

.PHONY: clean
clean:
	rm -f $(OBJ) ddos

.PHONY: cleandep
cleandep:
	rm -f $(DEP)
